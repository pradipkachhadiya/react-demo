import React, {Component} from 'react';
import {Provider} from 'react-redux';

import Navigation, { StartAppConst } from './src/Component/Navigation';
import store from './src/Store/store';

export default class App extends Component {
  render() {
    return (
      <Provider store={store}>
        <StartAppConst/>
      </Provider>
    );
  }
}


