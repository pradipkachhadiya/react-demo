/**
 * @format
 */

import { AppRegistry } from "react-native";
// import App from "./App";
import { name as appName } from "./app.json";

import Navigation, { StartAppConst } from "./src/Component/Navigation";
import React, { Component } from "react";
import { Provider } from "react-redux";
import store from "./src/Store/store";
import { createAppContainer } from "react-navigation";

const RootContainer = createAppContainer(StartAppConst)

export default class App extends Component {
  render() {
    return (
      <Provider store={store}>
        <RootContainer />
      </Provider>
    );
  }
}

AppRegistry.registerComponent(appName, () => App);
