import React, { Component } from "react";
import {
  ImageBackground,
  StatusBar,
  Image,
  View,
  TextInput,
  Text
} from "react-native";
import styles from "./../styles";
import { NavigationActions } from "react-navigation";

export default class LoginView extends Component {
  constructor(props) {
    super(props);
    this.state = {
      email: "",
      pass: ""
    };
  }

  // AppNavigator = createStackNavigator({
  //   Splash:{ screen : Welcome },

  // });

  login = () => {
    alert("Hello");
    const { userActions } = this.props;
    const { email, pass } = this.state;
    // userActions.loginRequest(email, pass);
    // this.props.navigation.navigate(Welcome);
    const navigateAction = NavigationActions.navigate({
      routeName: "Welcome",
      params: {
        email: email,
        pass: pass
      }
    });
    this.props.navigation.navigate(navigateAction);
  };

  render() {
    return (
      <ImageBackground
        style={styles.imagebg}
        source={require("./Images/screenbg.png")}
      >
        <View style={styles.container}>
          <StatusBar
            backgroundColor="transparent"
            barStyle="dark-content"
            translucent={true}
          />

          <Image
            style={[styles.logoimage, { marginTop: 80, marginBottom: 20 }]}
            source={require("./Images/logo.png")}
          />

          <TextInput
            style={styles.textinput}
            placeholder="Enter email"
            keyboardType="default"
            returnKeyType="next"
            onChangeText={email => this.setState({ email })}
            ref={ref => {
              this._email = ref;
            }}
            onSubmitEditing={event => {
              this._pass.focus();
            }}
          />
          <TextInput
            style={styles.textinput}
            placeholder="Enter password"
            keyboardType="default"
            returnKeyType="done"
            onChangeText={pass => this.setState({ pass })}
            ref={ref => {
              this._pass = ref;
            }}
          />

          <Text style={styles.button} onPress={() => this.login()}>
            LOGIN
          </Text>
        </View>
      </ImageBackground>
    );
  }
}
