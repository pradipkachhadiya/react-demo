import React, { Component } from "react";
import {
  StackNavigator,
  createStackNavigator,
  createAppContainer,
  addNavigationHelpers
} from "react-navigation";
import {
  createReduxContainer,
  createReduxBoundAddListener,
  createReactNavigationReduxMiddleware
} from "react-navigation-redux-helpers";
import LoginView from "./LoginView";
import { connect } from "react-redux";

export const StartAppConst = createStackNavigator({
  Login: {
    screen: LoginView
  },
  Welcome: {
    screen: LoginView
  }
});

// export const StartAppConst = createAppContainer(start);

export const middleware = createReactNavigationReduxMiddleware(
  "root",
  state => state.navigation
);

const addListener = createReduxContainer("root");

class Navigation extends Component {
  constructor(props) {
    super(props);
    // console.disableYellowBox = true;
  }
  render() {
    const navigation = addNavigationHelpers({
      dispatch: this.props.dispatch,
      state: this.props.navigation,
      addListener
    });

    return <StartAppConst navigation={navigation} />;
  }
}

const mapStateToProps = state => ({
  navigation: state.navigation
});

export default connect(mapStateToProps)(Navigation);
