import { createStore, applyMiddleware, combineReducers } from "redux";
import Navigation, { middleware } from "../Component/Navigation";
import logger from "redux-logger";
import NavigationReducer from "./../Reducer/NavigationReducer";

const rootReducer = combineReducers({NavigationReducer});

const store = createStore(rootReducer, applyMiddleware(logger, middleware));

export default store;
