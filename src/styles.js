import { StyleSheet,Dimensions } from "react-native";

const width = Dimensions.get('window').width;
const height = Dimensions.get('window').height;

const styles = StyleSheet.create({
    container : { 
        flex : 1,
        height : height,
        width : width
    },
    imagebg : {
        flex : 1,
        height : height,
        width : width
    },
    logoimage : {
        width : 150, 
        height : 150, 
        alignSelf : 'center'
    },
    textinput : {
        width: width,
        height: 42,
        alignSelf : 'center',
        fontSize: 16,
        color: "black",
        backgroundColor: 'white',
        marginLeft: "15%",
        marginRight: "15%",
        marginTop: 10
    },
    button : {
        width: width,
        padding: 8,
        alignSelf : 'center',
        color : 'black',
        fontWeight: 'bold',  
        fontSize : 18,
        backgroundColor: 'white',
        marginLeft: "15%",
        marginRight: "15%",
        marginTop: 30,
        textAlign : 'center'
    }
});

export default styles;